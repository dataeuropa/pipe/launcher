package io.piveau.pipe

import io.vertx.core.Vertx
import io.vertx.core.json.JsonObject
import io.vertx.junit5.VertxExtension
import io.vertx.junit5.VertxTestContext
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith

@ExtendWith(VertxExtension::class)
class LauncherTest {

    private val clusterConfig = JsonObject(
        """
            {
                "pipeRepositories": {
                    "system": {
                        "uri": "https://dummy.org",
                        "username": "dummy",
                        "token": "dummy",
                        "branch": "master"
                    }
                },
                "serviceDiscovery": {
                    "test-segment": {
                        "endpoints": {
                            "http": {
                                "address": "http://localhost:8098/pipe"
                            },
                            "eventbus": {
                                "address": "piveau.pipe.test1.queue"
                            }
                        }
                    }
                }
            }
        """.trimIndent()
    )

    @Test
    fun clusterConfiguration(vertx: Vertx, testContext: VertxTestContext) {
        PiveauCluster.create(vertx, clusterConfig).onSuccess {
            assert(it.availablePipeRepos().contains("system"))
            testContext.completeNow()
        }.onFailure { testContext.failNow(it) }
    }

    @Test
    fun launcherPipe(vertx: Vertx, testContext: VertxTestContext) {
        val checkpoint = testContext.checkpoint(2)

        vertx.createHttpServer().requestHandler {
            it.bodyHandler { body -> println(body) }
            it.response().setStatusCode(202).end()
            checkpoint.flag()
        }.listen(8098)

        PiveauCluster.create(vertx, clusterConfig)
            .compose { cluster -> cluster.pipeLauncher(vertx) }
            .compose { launcher -> launcher.runPipe("test1", JsonObject(), null) }
            .onSuccess { checkpoint.flag() }
            .onFailure { testContext.failNow(it) }
    }

    @Test
    fun launcherPipeWithData(vertx: Vertx, testContext: VertxTestContext) {
        val checkpoint = testContext.checkpoint(2)

        vertx.createHttpServer().requestHandler {
            it.bodyHandler { body -> println(body) }
            it.response().setStatusCode(202).end()
            checkpoint.flag()
        }.listen(8098)

        PiveauCluster.create(vertx, clusterConfig)
            .compose { cluster -> cluster.pipeLauncher(vertx) }
            .compose { launcher -> launcher.runPipeWithData("test1", "https://obchodní-rejstřík.stirdata.opendata.cz/soubor/or.hdt", "text/plain") }
            .onSuccess { checkpoint.flag() }
            .onFailure { cause -> testContext.failNow(cause) }
    }

    @Test
    fun serviceDiscovery(vertx: Vertx, testContext: VertxTestContext) {
        PiveauCluster.create(vertx, clusterConfig).onSuccess { cluster ->
            val endpoints = cluster.serviceDiscovery.resolve("test-segment")
            assert(endpoints.getJsonObject("http").getString("address") == "http://localhost:8098/pipe")

            val endpoint = cluster.serviceDiscovery.resolve("test-segment", "eventbus")
            assert(endpoint.getString("address") == "piveau.pipe.test1.queue")

            testContext.completeNow()
        }.onFailure { testContext.failNow(it) }
    }

}
