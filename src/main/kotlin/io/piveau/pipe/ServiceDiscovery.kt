package io.piveau.pipe

import io.vertx.core.json.JsonObject
import org.slf4j.LoggerFactory

class ServiceDiscovery(private val config: JsonObject) {
    private val log = LoggerFactory.getLogger(javaClass)

    init {
        if (config.isEmpty) log.warn("Empty service discovery configured!")
    }

    fun resolve(name: String): JsonObject =
        config.getJsonObject(name, JsonObject()).getJsonObject("endpoints", JsonObject())

    fun resolve(name: String, type: String): JsonObject =
        config
            .getJsonObject(name, JsonObject())
            .getJsonObject("endpoints", JsonObject())
            .getJsonObject(type)
            ?: if (type == "http") JsonObject()
                // default endpoint
                .put("protocol", type)
                .put("address", "http://$name:8080/pipe")
                .put("method", "POST")
            else JsonObject()
                // set at least the protocol
                .put("protocol", type)
}
