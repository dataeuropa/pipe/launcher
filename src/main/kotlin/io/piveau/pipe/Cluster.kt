package io.piveau.pipe

import io.piveau.pipe.repositories.GitRepository
import io.vertx.core.*
import io.vertx.core.json.JsonObject
import io.vertx.kotlin.coroutines.await
import org.slf4j.LoggerFactory

class PiveauCluster private constructor(config: JsonObject) {

    private val log = LoggerFactory.getLogger(this::class.java)

    companion object {

        private var cluster: PiveauCluster? = null

        @JvmStatic
        @JvmOverloads
        fun create(vertx: Vertx, config: JsonObject, override: Boolean = false): Future<PiveauCluster> {
            return vertx.executeBlocking { promise ->
                val cl = if (override || cluster == null) PiveauCluster(config) else cluster
                promise.complete(cl)
            }
        }

    }

    val serviceDiscovery = ServiceDiscovery(config.getJsonObject("serviceDiscovery", JsonObject()))

    internal val repos = mutableMapOf<String, GitRepository>()

    init {
        val pipeRepositories = config.getJsonObject("pipeRepositories", JsonObject())
        if (pipeRepositories.isEmpty) {
            log.warn("No pipe repositories configured!")
        } else {
            pipeRepositories.stream().filter { it.value is JsonObject }.forEach {
                with(it.value as JsonObject) {
                    repos[it.key] = GitRepository(
                        getString("uri"),
                        getString("branch"),
                        getString("username"),
                        getString("token"),
                        getString("path", ".")
                    )
                }
            }
        }
    }

    fun pipeLauncher(vertx: Vertx): Future<PipeLauncher> = PipeLauncher.create(vertx, this)

    fun availablePipeRepos() = repos.keys.toList()

}
