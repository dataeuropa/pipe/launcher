package io.piveau.pipe

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.json.JsonMapper
import com.fasterxml.jackson.databind.node.ObjectNode
import io.piveau.pipe.model.*
import io.vertx.core.Future
import io.vertx.core.Promise
import io.vertx.core.Vertx
import io.vertx.core.buffer.Buffer
import io.vertx.core.file.CopyOptions
import io.vertx.core.http.HttpHeaders
import io.vertx.core.http.HttpMethod
import io.vertx.core.http.HttpResponseExpectation
import io.vertx.core.json.Json
import io.vertx.core.json.JsonArray
import io.vertx.core.json.JsonObject
import io.vertx.core.json.pointer.JsonPointer
import io.vertx.ext.web.client.WebClient
import io.vertx.ext.web.codec.BodyCodec
import io.vertx.kotlin.coroutines.await
import io.vertx.kotlin.coroutines.coAwait
import io.vertx.kotlin.coroutines.setPeriodicAwait
import io.vertx.kotlin.coroutines.vertxFuture
import org.eclipse.jgit.api.MergeResult
import org.slf4j.LoggerFactory
import java.net.URL
import java.util.*
import kotlin.reflect.cast
import kotlin.streams.toList

class PipeLauncher private constructor(val vertx: Vertx, private val cluster: PiveauCluster) {

    private val log = LoggerFactory.getLogger(this.javaClass)

    private val client = WebClient.create(vertx)

    private val pipes = mutableMapOf<String, Pipe>()

    private val pipeNamePointer = JsonPointer.from("/pipe/header/name")

    init {
        vertx.setPeriodicAwait(vertx.orCreateContext.config().getLong("PIVEAU_LAUNCHER_PERIODIC_UPDATE", 20000)) {
            log.trace("Launcher periodic update trigger.")
            if (cluster.repos.values.any { it.pullRepo() != MergeResult.MergeStatus.ALREADY_UP_TO_DATE }) {
                updatePipes(vertx)
            }
        }
    }

    private suspend fun updatePipes(vertx: Vertx) {
        pipes.clear()
        cluster.repos.values.forEach {
            readPipes(vertx, it.pipeFiles().map { f -> f.absolutePath })
        }
        when {
            vertx.fileSystem().exists("pipes").coAwait() -> {
                val files =
                    vertx.fileSystem().readDir("pipes", "^([a-zA-Z0-9\\s_\\\\.\\-\\(\\):])+\\.(json|yaml|yml)\$")
                        .coAwait()
                readPipes(vertx, files)
            }

            else -> log.trace("No local pipe directory")
        }
    }

    @JvmOverloads
    fun runPipeWithBinaryData(
        pipeName: String,
        data: ByteArray,
        mimeType: String? = null,
        info: JsonObject? = null,
        configs: JsonObject = JsonObject(),
        runId: String? = null
    ): Future<JsonObject> = deepPipeCopy(pipeName)?.let {
        it.body.segments.minByOrNull { segment -> segment.header.segmentNumber }?.apply {
            body.payload = Payload(
                header = PayloadHeader(
                    dataType = DataType.base64,
                    seqNumber = 0
                ),
                body = PayloadBody(
                    data = Base64.getEncoder().encodeToString(data),
                    dataMimeType = mimeType,
                    dataInfo = info?.let { i -> ObjectMapper().readTree(i.encode()) as ObjectNode }
                )
            )
        }
        runPipe(it, configs, runId)
    } ?: Future.failedFuture("No such pipe!")

    @JvmOverloads
    fun runPipeWithData(
        pipeName: String,
        data: String,
        mimeType: String? = null,
        info: JsonObject? = null,
        configs: JsonObject = JsonObject(),
        runId: String? = null
    ): Future<JsonObject> = deepPipeCopy(pipeName)?.let {
        it.body.segments.minByOrNull { segment -> segment.header.segmentNumber }?.apply {
            body.payload = Payload(
                header = PayloadHeader(
                    dataType = DataType.text,
                    seqNumber = 0
                ),
                body = PayloadBody(
                    data = data,
                    dataMimeType = mimeType,
                    dataInfo = info?.let { i -> ObjectMapper().readTree(i.encode()) as ObjectNode }
                )
            )
        }
        runPipe(it, configs, runId)
    } ?: Future.failedFuture("No such pipe!")

    @JvmOverloads
    fun runPipe(pipeName: String, configs: JsonObject = JsonObject(), runId: String?): Future<JsonObject> =
        deepPipeCopy(pipeName)?.let {
            runPipe(it, configs, runId)
        } ?: Future.failedFuture("No such pipe!")

    private fun runPipe(pipe: Pipe, configs: JsonObject = JsonObject(), runId: String?): Future<JsonObject> {
        pipe.header.runId = runId
        pipe.body.segments.forEach { (header, body) ->
            if (configs.containsKey(header.name)) {
                val config = JsonObject(body.config?.toString() ?: "").mergeIn(configs.getJsonObject(header.name), true)
                body.config = ObjectMapper().readTree(config.encode()) as ObjectNode
            }
        }

        resolveEndpoints(pipe)

        return sendPipe(pipe)
    }

    fun isPipeAvailable(name: String) = pipes.contains(name)

    fun availablePipes() = pipes.values.toList()

    fun getPipe(name: String) = pipes[name]!!

    fun addPipe(source: String, filename: String): Future<JsonObject> = vertxFuture {
        vertx.fileSystem().move(source, "pipes/$filename", CopyOptions().setReplaceExisting(true)).await()
        updatePipes(vertx)
        JsonObject()
            .put("status", "added")
            .put("location", "")
    }

    private fun deepPipeCopy(name: String) = pipes[name]?.let { pipe ->
        readPipe(pipe.prettyPrint())
    }

    private fun sendPipe(pipe: Pipe): Future<JsonObject> {
        val pipeManager = PipeManager.manage(pipe)
        val endpoint = pipeManager.currentEndpoint ?: return Future.failedFuture("No endpoint available")

        val buffer = Buffer.buffer(pipeManager.prettyPrint())

        val address = URL(endpoint.address!!)

        val promise = Promise.promise<JsonObject>()
        client.request(HttpMethod.valueOf(endpoint.method ?: "POST"), address.port, address.host, address.path)
            .putHeader(HttpHeaders.CONTENT_TYPE.toString(), "application/json")
            .sendBuffer(buffer)
            .onSuccess {
                when (it.statusCode()) {
                    200, 202 -> {
                        val contentType = it.getHeader(HttpHeaders.CONTENT_TYPE.toString())
                        if (contentType != null && contentType.contains("application/json")) {
                            promise.complete(it.bodyAsJsonObject())
                        } else {
                            promise.complete(JsonObject().put("runId", it.bodyAsString()))
                        }
                    }

                    else -> promise.fail("${it.statusCode()} - ${it.statusMessage()}")
                }
            }
            .onFailure(promise::fail)

        return promise.future()
    }

    fun listRuns(): Future<List<JsonObject>> {
        val requests = listStartEndpoints().map { endpoint ->
            client.getAbs("${endpoint.address}/runs")
                .`as`(BodyCodec.jsonArray())
                .putHeader(HttpHeaders.ACCEPT.toString(), "application/json")
                .send()
                .expecting(HttpResponseExpectation.SC_OK.and(HttpResponseExpectation.JSON))
                .map { it.body() }
        }
        return Future.join(requests)
            .transform {
                Future.succeededFuture(
                    requests.stream()
                        .filter { it.succeeded() }
                        .map { it.result() }
                        .map { entry ->
                            entry.mapNotNull { e ->
                                when {
                                    e is JsonObject -> e
                                    else -> null
                                }
                            }
                        }
                        .toList()
                        .flatten()
                )
            }
    }

    fun listPipeRuns(pipeName: String): Future<JsonArray> {
        val pipeManager = PipeManager.manage(deepPipeCopy(pipeName)!!)
        resolveEndpoints(pipeManager.pipe)
        return if (pipeManager.currentEndpoint != null) {
            client.getAbs("${pipeManager.currentEndpoint!!.address}/runs")
                .`as`(BodyCodec.jsonArray())
                .send()
                .expecting(HttpResponseExpectation.SC_OK.and(HttpResponseExpectation.JSON))
                .map { response ->
                    JsonArray(
                        response.body().filter { pipeNamePointer.queryJson(it as JsonObject).toString() == pipeName })
                }
        } else {
            Future.failedFuture("No endpoint available!")
        }
    }

    fun getRunStatus(pipeName: String, runId: String): Future<JsonObject> {
        val pipeManager = PipeManager.manage(deepPipeCopy(pipeName)!!)
        resolveEndpoints(pipeManager.pipe)
        return if (pipeManager.currentEndpoint != null) {
            client.getAbs("${pipeManager.currentEndpoint!!.address}/$runId")
                .`as`(BodyCodec.jsonObject())
                .send()
                .expecting(HttpResponseExpectation.SC_OK.and(HttpResponseExpectation.JSON))
                .map { it.body() }
        } else {
            Future.failedFuture("No endpoint available!")
        }
    }

    fun cancelRun(pipeName: String, runId: String): Future<Void> {
        val pipeManager = PipeManager.manage(deepPipeCopy(pipeName)!!)
        resolveEndpoints(pipeManager.pipe)
        return if (pipeManager.currentEndpoint != null) {
            client.deleteAbs("${pipeManager.currentEndpoint!!.address}/$runId")
                .send()
                .expecting(HttpResponseExpectation.SC_NO_CONTENT)
                .mapEmpty()
        } else {
            Future.failedFuture("No endpoint available!")
        }
    }

    private fun listStartEndpoints(): List<Endpoint> {
        return pipes.keys.mapNotNull { pipeName ->
            val pipe = deepPipeCopy(pipeName)!!
            resolveEndpoints(pipe)
            PipeManager.manage(pipe).currentEndpoint
        }.distinct()
    }

    private suspend fun readPipes(vertx: Vertx, files: List<String>) = files.forEach {
        try {
            val pipe = when {
                it.endsWith(".json") -> {
                    val content = vertx.fileSystem().readFile(it).coAwait().toString();
                    val manager = PipeManager.readJson(content)
                    manager.pipe
                }

                else -> PipeManager.readYaml(vertx.fileSystem().readFile(it).coAwait().toString()).pipe
            }
            pipes[pipe.header.name] = pipe
        } catch (t: Throwable) {
            log.warn("Reading pipe from {} failed", it, t)
        }
    }

    private fun resolveEndpoints(pipe: Pipe) {
        pipe.body.segments.forEach { (header, body) ->
            if (body.endpoint == null) {
                val endpoint = cluster.serviceDiscovery.resolve(header.name, "http")
                body.endpoint = Json.decodeValue(endpoint.toString(), Endpoint::class.java)
            }
        }
    }

    companion object {
        fun create(vertx: Vertx, cluster: PiveauCluster) = vertxFuture {
            PipeLauncher(vertx, cluster).apply {
                updatePipes(vertx)
            }
        }
    }
}
