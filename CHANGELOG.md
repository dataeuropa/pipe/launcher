# ChangeLog

## Unreleased

## 4.1.2 (2024-06-06)

**Fixed:**
* Dependency chaos with jackson library

## 4.1.1 (2024-06-04)

**Fixed:**
* Using list of requests instead of `list()` method from composite future

## 4.1.0 (2024-05-31)

**Changed:**
* Improved run API

## 4.0.5 (2024-04-15)

**Fixed:**
* Empty git access token handling

**Changed:**
* Generate overloaded functions in launcher
* Lib dependency updates

## 4.0.4 (2024-02-14)

**Fixed:**
* Vert.x update ([CVE-2024-1300](https://access.redhat.com/security/cve/CVE-2024-1300))

## 4.0.3 (2023-11-14)

Publishing release

## 4.0.2 (2023-11-11)

**Changed:**
* Update model library with increased json string size limit

## 4.0.1 (2023-05-14)

**Added:**
* Add pipe method

## 4.0.0 (2023-04-10)

**Changed:**
* Launcher creation is now asynchronous
* Default periodic update time to every 20 seconds
* Updates are now non-blocking
* `README.md`

## 3.0.2 (2023-02-12)

**Changed:**
* Bump up lib dependencies

## 3.0.1 (2022-11-20)

**Changed:**
* Lib updates

## 3.0.0 (2022-10-11)

**Changed:**
* Launching a pipe works now on a deep copy of the original pipe

**Added:**
* The ability to receive a json info object as response
* Get run status and cancel a run

## 2.2.5 (2022-04-28)

**Changed:**
* Dependency updates

## 2.2.4 (2022-01-09)

**Changed:**
* Another pipe model lib update
* General dependencies update

## 2.2.3 (2021-06-10)

**Changed:**
* Update pipe model lib

## 2.2.2 (2021-06-02)

**Changed:**
* Lib dependencies update

## 2.2.1 (2021-05-05)

**Fixed:**
* Maintenance

## 2.2.0 (2021-02-06)

**Changed:**
* Switched to Vert.x 4.0.2

## 2.1.0 (2020-07-30)

**Added**:
* Support for yaml pipe definitions
* Support for path in git repository

**Fixed:**
* Exceptions during pipe read

## 2.0.1 (2020-07-13)

**Fixed:**
* Service discovery default http endpoint json object

## 2.0.0 (2020-06-11)

**Added:**
* Ability to re-create the cluster with a new config

**Changed:**
* Use new model lib

## 1.1.1 (2020-04-02)

**Changed:**
* Vert.x and dependencies update

## 1.1.0 (2020-03-05)

**Changed:**
* Return run id after launching pipe

**Fixed:**
* Apply segment configs

## 1.0.3 (2020-03-03)

???

## 1.0.2 (2020-02-28)

**Changed:**
* Initialization of cluster only once allowed
* JsonObject for dataInfo
* Service discovery returns default http build from name

## 1.0.1 (2019-12-10)

**Added:**
* Method to get a single pipe

## 1.0.0 (2019-10-02)

**Fixed:**
* first segment selection

## 0.1.0 (2019-08-26)

Initial release
